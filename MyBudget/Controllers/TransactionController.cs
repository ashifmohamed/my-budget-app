﻿using MyBudget.Controllers.Interface;
using MyBudget.Core;
using MyBudget.Models.Transactions;
using MyBudget.Services;
using MyBudget.Views.Interface;
using MyBudget.Views.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Controllers
{
    public class TransactionController : ITransactionController
    {
        ITransactionView _transactionView;
        TransactionService _transactionService = new TransactionService();

        public TransactionController()
        {

        }
        public TransactionController(ITransactionView transactionView)
        {
            _transactionView = transactionView;
            _transactionView.SetController(this);
        }
        public async void AddTransactions(IList<BasicTransaction> transactions)
        {
            bool isAdded = await _transactionService.AddTransactions(transactions);

            if (isAdded)
            {
                _transactionView.ShowMessage("Transactions Added successfully.");
                _transactionView.FilerData();
            }
            else
            {
                _transactionView.ShowMessage("There was an error when adding a Transaction.");
            }
        }

        public async Task<TransactionDTO> GetTransaction(int id)
        {
            return await _transactionService.GetTransactionAsync(id);
        }

        public async Task<IList<BasicTransaction>> GetTransactions()
        {
            return await _transactionService.GetAllTransactionsAsync();
        }

        public void InitializeData()
        {
            _transactionService.InitializeData();
        }

        public async void LoadTransactions()
        {
            var transactionList = await GetTransactions();
            var mappedList = MapViewData(transactionList);

            _transactionView.LoadTransactions(mappedList);
        }

        public Task<BasicTransaction> RemoveTransaction(int id)
        {
            return _transactionService.RemoveTransactionAsync(id);
        }

        public async void UpdateTransaction(TransactionDTO transaction)
        {
            bool isUpdated = await _transactionService.UpdateTransactionAsync(transaction);
            if (isUpdated)
            {
                _transactionView.ShowMessage("Transaction Updated successfully.");
                _transactionView.FilerData();
            }
            else
            {
                _transactionView.ShowMessage("There was an error while updating the Transaction.");
            }
        }
        //public void GetFilteredTransactions(DateTime start, DateTime end, BasicTransaction.TRANSACTION_TYPE type, bool isRecurring, bool isCredit)
        //{
        //    IList <BasicTransaction> data = _transactionService.GetFilteredTransactions(start, end, type, isRecurring, isCredit);
        //    IList<TransactionViewData> viewData = MapViewData(data);

        //    _transactionView.SetFilteredTransactions(data);
        //    _transactionView.LoadTransactions(viewData);
        //}
        public async void GetTransactions(DateTime start, DateTime end)
        {
            //IList<BasicTransaction> data = await _transactionService.GetAllTransactionsAsync(start, end);
            //IList<TransactionViewData> viewData = MapViewData(data);

            //_transactionView.SetFilteredTransactions(data);
            //_transactionView.LoadTransactions(viewData);
        }

        public void ShowTransactions(IList<BasicTransaction> transactions)
        {
            //IList<TransactionViewData> viewData = MapViewData(transactions);

            //_transactionView.SetFilteredTransactions(transactions);
            //_transactionView.LoadTransactions(viewData);
        }
        private IList<TransactionViewData> MapViewData(IList<BasicTransaction> data)
        {
            return data.Select(x => new TransactionViewData()
            {
                Id = x.Id,
                Amount = x.Amount,
                Date = x.Date.ToString(ThemeConstant.DATE_FORMAT),
                Type = (int)x.Type,
                Category = x.Category.Name
            }).ToList();
        }

        public async void GetFilteredTransactions(DateTime start, DateTime end, TransactionViewData.TYPE type)
        {
            List<TransactionDTO> data = await _transactionService.GetFilteredTransactionsAsync(start, 
                end, type);

            TransactionList listOfTransactions = new TransactionList(new List<BasicTransaction>(),
                new List<CreditTransaction>(), new List<RecurTransaction>());

            data.ForEach(transactions =>
            {
                listOfTransactions.Add(transactions.MapToObject());
            });

            List<BasicTransaction> transactionList = new List<BasicTransaction>();
            data.ForEach(transactions =>
            {
                transactionList.Add(transactions.MapToObject());
            });

            IList<TransactionViewData> viewData = MapViewData(transactionList);

            _transactionView.SetFilteredTransactions(listOfTransactions);
            _transactionView.LoadTransactions(viewData);
        }

        public async void AddTransactions(TransactionList transactions)
        {
            bool isAdded = await _transactionService.AddTransactions(transactions);

            if (isAdded)
            {
                _transactionView.ShowMessage("Transactions Added successfully.");
                _transactionView.FilerData();
            }
            else
            {
                _transactionView.ShowMessage("There was an error when adding a Transaction.");
            }
        }
    }
}
