﻿using MyBudget.Controllers.Interface;
using MyBudget.Data;
using MyBudget.Models;
using MyBudget.Models.Transactions;
using MyBudget.Services;
using MyBudget.Views.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MyBudget.Models.Dashboard;

namespace MyBudget.Controllers
{
    public class DashboardController : IDashboardController
    {
        IDashboardView _dashboardView;
        DashboardService _dashboardService = new DashboardService();

        public DashboardController()
        {

        }
        public DashboardController(IDashboardView dashboardView)
        {
            _dashboardView = dashboardView;
            _dashboardView.SetController(this);
        }

        public async void GetTransactionChartData(DateTime start, 
            DateTime end, CHART_GROUP group)
        {
            IList<TranscationChartData> data = await _dashboardService.GetTransactionChartData(start, end, group);

            if(group == CHART_GROUP.MONTH)
            {
                data.Select(d => { d.DateGroup = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Int32.Parse(d.DateGroup)); return d; }).ToList();
            }
            else if (group == CHART_GROUP.DAILY)
            {
                data.Select(d => { d.DateGroup = Convert.ToDateTime(d.DateGroup).ToString("MMM dd yyyy"); return d; }).ToList();
            }

            _dashboardView.LoadTransactionChart(data); 
        }

        public Task<Double> GetForecastBalance(DateTime currentDate, BasicTransaction.TRANSACTION_TYPE type)
        {
            return _dashboardService.GetForecastBalance(currentDate, type);
        }
        public Task<Double> GetActualBalance(DateTime currentDate, BasicTransaction.TRANSACTION_TYPE type)
        {
            return _dashboardService.GetActualBalance(currentDate, type);
        }

        public Task<IList<CategoryMonthTotalData>> GetCategoryChartData(DateTime date)
        {
            return _dashboardService.GetCategoryChartData(date);
        }
    }
}
