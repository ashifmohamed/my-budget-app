﻿using MyBudget.Controllers.Interface;
using MyBudget.Models;
using MyBudget.Services;
using MyBudget.Views.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Controllers
{
    public class ContactController : IContactController
    {
        IContactView _contactView;
        ContactService _contactService = new ContactService();

        public ContactController()
        {

        }
        public ContactController(IContactView contactView)
        {
            _contactView = contactView;
            _contactView.SetController(this);
        }
        public async void AddContact(string name)
        {
            bool isAdded = await _contactService.AddContactAsync(name);

            if (isAdded)
            {
                _contactView.ShowMessage("Contact Added successfully.");
                LoadContacts();
            }
            //else
            //{
            //    _contactView.ShowMessage("There is already a Contact with same name.");
            //}

        }

        public async Task<IList<Contact>> GetContacts()
        {
           return await _contactService.GetAllContactsAsync();
        }

        public void InitializeData()
        {
            _contactService.InitializeData();
        }

        public async void LoadContacts()
        {
            var contactList = await GetContacts();
            _contactView.LoadContacts(contactList);
        }

        public async void UpdateContact(Contact contact)
        {
            bool isUpdated = await _contactService.UpdateContactAsync(contact);

            if (isUpdated)
            {
                _contactView.ShowMessage("Contact Updated successfully.");
                LoadContacts();
            }
            //else
            //{
            //    _contactView.ShowMessage("There was an error while updating the contact.");
            //}
        }
    }
}
