﻿using MyBudget.Models;
using MyBudget.Models.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MyBudget.Models.Dashboard;
using static MyBudget.Models.Transactions.BasicTransaction;

namespace MyBudget.Controllers.Interface
{
    public interface IDashboardController
    {
        void GetTransactionChartData(DateTime start,
            DateTime end, CHART_GROUP group);
        Task<IList<CategoryMonthTotalData>> GetCategoryChartData(DateTime date);

        Task<Double> GetForecastBalance(DateTime currentDate, TRANSACTION_TYPE type);
        Task<Double> GetActualBalance(DateTime currentDate, TRANSACTION_TYPE type);

    }
}
