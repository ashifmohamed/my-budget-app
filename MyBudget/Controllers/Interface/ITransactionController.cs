﻿using MyBudget.Models.Transactions;
using MyBudget.Views.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MyBudget.Models.Transactions.BasicTransaction;

namespace MyBudget.Controllers.Interface
{
    public interface ITransactionController
    {
        void InitializeData();
        void LoadTransactions();
        Task<IList<BasicTransaction>> GetTransactions();
        void GetTransactions(DateTime start, DateTime end);
        void ShowTransactions(IList<BasicTransaction> transactions);
        Task<TransactionDTO> GetTransaction(int id);

        void AddTransactions(IList<BasicTransaction> transactions);
        void AddTransactions(TransactionList transactions);

        void UpdateTransaction(TransactionDTO transaction);
        Task<BasicTransaction> RemoveTransaction(int id);
        //void GetFilteredTransactions(DateTime start, DateTime end,
        //    TRANSACTION_TYPE type, bool isRecurring, bool isCredit);
        void GetFilteredTransactions(DateTime start, DateTime end,
            TransactionViewData.TYPE type);
    }
}
