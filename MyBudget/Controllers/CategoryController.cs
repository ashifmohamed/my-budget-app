﻿using MyBudget.Controllers.Interface;
using MyBudget.Models;
using MyBudget.Services;
using MyBudget.Views.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Controllers
{
    public class CategoryController : ICategoryController
    {
        ICategoryView _categoryView;
        CategoryService _categoryService = new CategoryService();

        public CategoryController()
        {

        }
        public CategoryController(ICategoryView categoryView)
        {
            _categoryView = categoryView;
            _categoryView.SetController(this);
        }

        public async void AddCategory(string name)
        {
            bool isAdded = await _categoryService.AddCategoryAsync(name);

            if (isAdded)
            {
                _categoryView.ShowMessage("Category Added successfully.");
                LoadCategories();
            }
            //else
            //{
            //    _categoryView.ShowMessage("There is already a Category with same name.");
            //}
        }

        public async Task<IList<Category>> GetCategories()
        {
            return  await _categoryService.GetAllCategoriesAsync();

            //return categories.ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public void InitializeData()
        {
            _categoryService.InitializeData();
        }

        public async void LoadCategories()
        {
            var categoryList = await GetCategories();
            _categoryView.LoadCategories(categoryList);
        }

        public async void UpdateCategory(Category category)
        {
            bool isUpdated = await _categoryService.UpdateCategoryAsync(category);
            if (isUpdated)
            {
                _categoryView.ShowMessage("Category Updated successfully.");
                LoadCategories();
            }
            //else
            //{
            //    _categoryView.ShowMessage("There was an error while updating the category.");
            //}
        }
    }
}