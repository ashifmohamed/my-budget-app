﻿using MyBudget.Controllers.Interface;
using MyBudget.Models;
using MyBudget.Models.Transactions;
using MyBudget.Views.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Views.Interface
{
    public interface ITransactionView
    {
        void SetController(ITransactionController controller);
        void AddTransactions(IList<BasicTransaction> transactions);
        void AddTransactions(TransactionList transactions);
        void UpdateTransaction(TransactionDTO transaction);
        void LoadTransactions(IList<TransactionViewData> transactionList);
        void SetFilteredTransactions(TransactionList transactionList);
        void FilerData();
        void ShowMessage(string message);
        IList<Contact> Contacts { get; }
        IList<Category> Categories { get; }  
    }
}
