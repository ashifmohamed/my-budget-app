﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyBudget.Views.Transaction
{
    public partial class ViewImportTransaction : Form
    {
        public ViewImportTransaction()
        {
            InitializeComponent();
        }

        public ViewImportTransaction(string importedText)
        {
            InitializeComponent();

            txt_import.Text = importedText;
        }
    }
}
