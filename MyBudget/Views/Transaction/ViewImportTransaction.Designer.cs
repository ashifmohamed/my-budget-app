﻿namespace MyBudget.Views.Transaction
{
    partial class ViewImportTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_back = new System.Windows.Forms.Panel();
            this.txt_import = new System.Windows.Forms.RichTextBox();
            this.pnl_back.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_back
            // 
            this.pnl_back.Controls.Add(this.txt_import);
            this.pnl_back.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_back.Location = new System.Drawing.Point(0, 0);
            this.pnl_back.Name = "pnl_back";
            this.pnl_back.Size = new System.Drawing.Size(726, 552);
            this.pnl_back.TabIndex = 0;
            // 
            // txt_import
            // 
            this.txt_import.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_import.Location = new System.Drawing.Point(0, 0);
            this.txt_import.Name = "txt_import";
            this.txt_import.Size = new System.Drawing.Size(726, 552);
            this.txt_import.TabIndex = 0;
            this.txt_import.Text = "";
            // 
            // ViewImportTransaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 552);
            this.Controls.Add(this.pnl_back);
            this.Name = "ViewImportTransaction";
            this.pnl_back.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_back;
        private System.Windows.Forms.RichTextBox txt_import;
    }
}