﻿using MyBudget.Core;
using MyBudget.Models;
using MyBudget.Models.Transactions;
using MyBudget.Views.Interface;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using static MyBudget.Models.Transactions.BasicTransaction;
using static MyBudget.Models.Transactions.RecurTransaction;

namespace MyBudget.Views.Transaction
{
    public partial class UpdateTransaction : Form
    {
        private ITransactionView _transactionView;
        private BasicTransaction _transaction;
        public UpdateTransaction()
        {
            InitializeComponent();
        }

        public UpdateTransaction(ITransactionView transactionView, BasicTransaction transaction)
        {
            InitializeComponent();
            _transactionView = transactionView;
            _transaction = transaction;

            InitializeComboBoxes();
            InitializeControls();
        }
        private void InitializeComboBoxes()
        {
            cmb_category.DisplayMember = "Name";
            cmb_category.ValueMember = "Id";
            cmb_contact.DisplayMember = "Name";
            cmb_contact.ValueMember = "Id";
            cmb_contact.DataSource = new List<Contact>(_transactionView.Contacts);
            cmb_category.DataSource = new List<Category>(_transactionView.Categories);
            cmb_type.Items.AddRange(Enum.GetNames(typeof(TransactionViewData.TYPE)));
            cmb_cycle.Items.AddRange(Enum.GetNames(typeof(TransactionCycle)));
            cmb_type.Format += new ListControlConvertEventHandler(typeComboBox_Format);

        }
        private void InitializeControls()
        {
            txt_amount.Text = _transaction.Amount.ToString();

            txt_amount.KeyPress += new KeyPressEventHandler(amount_KeyPress);
            txt_amount.Leave += new EventHandler(amount_Leave);

            date_transaction.Value = _transaction.Date;
            date_transaction.Format = DateTimePickerFormat.Custom;
            date_transaction.CustomFormat = ThemeConstant.DATE_FORMAT;
            date_expected.Format = DateTimePickerFormat.Custom;
            date_expected.CustomFormat = ThemeConstant.DATE_FORMAT;
            date_end.Format = DateTimePickerFormat.Custom;
            date_end.CustomFormat = ThemeConstant.DATE_FORMAT;
            cmb_category.SelectedValue = _transaction.Category.Id;

            TransactionViewData.TYPE type;
            if (_transaction is CreditTransaction creditTransaction)
            {
                type = creditTransaction.Type == TRANSACTION_TYPE.INCOME ?
                                            TransactionViewData.TYPE.RECEIVABLE : 
                                            TransactionViewData.TYPE.PAYABALE;
                EnablePanelContents(pnl_recur, false);
                cmb_contact.SelectedValue = creditTransaction.Contact.Id;

                //cmb_contact.SelectedIndex = cmb_contact.Items.IndexOf(creditTransaction.Contact);
                date_expected.Value = creditTransaction.ExpectedDate;
            }
            else if(_transaction is RecurTransaction recurTransaction)
            {
                type = recurTransaction.Type == TRANSACTION_TYPE.INCOME ?
                                            TransactionViewData.TYPE.RECURRING_INCOME :
                                            TransactionViewData.TYPE.RECURRING_EXPENSE;
                EnablePanelContents(pnl_contact, false);

                string cycle = recurTransaction.Cycle.ToString();
                cmb_cycle.SelectedItem = cycle;

                date_end.Value = recurTransaction.ExpectedEndDate;
            }
            else
            {
                type = _transaction.Type == TRANSACTION_TYPE.INCOME ?
                                            TransactionViewData.TYPE.INCOME :
                                            TransactionViewData.TYPE.EXPENSE;
                EnablePanelContents(pnl_contact, false);
                EnablePanelContents(pnl_recur, false);
            }

            cmb_type.SelectedItem = type.ToString();
        }

        private void comboBox1_Format(object sender, ListControlConvertEventArgs e)
        {

        }

        private void lbl_type_Click(object sender, EventArgs e)
        {

        }

        private void cmb_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedType = (string)cmb_type.SelectedItem;

            if (Enum.TryParse(selectedType, out TransactionViewData.TYPE transactionType))
            {
                switch (transactionType)
                {
                    case TransactionViewData.TYPE.RECURRING_INCOME:
                    case TransactionViewData.TYPE.RECURRING_EXPENSE:
                        EnablePanelContents(pnl_recur, true);
                        EnablePanelContents(pnl_contact, false);
                        break;

                    case TransactionViewData.TYPE.PAYABALE:
                    case TransactionViewData.TYPE.RECEIVABLE:
                        EnablePanelContents(pnl_contact, true);
                        EnablePanelContents(pnl_recur, false);
                        break;
                    default:
                        EnablePanelContents(pnl_contact, false);
                        EnablePanelContents(pnl_recur, false);
                        break;
                }
            }
        }

        private void lbl_expected_Click(object sender, EventArgs e)
        {

        }
        private void EnablePanelContents(Panel panel, bool enabled)
        {
            foreach (Control control in panel.Controls)
            {
                control.Enabled = enabled;
            }

            panel.Visible = enabled;
        }

        private void amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void amount_Leave(object sender, EventArgs e)
        {
            TextBox control = sender as TextBox;

            if (double.TryParse(control.Text, out double value))
            {
                control.Text = string.Format("{0:#,##0.00}", value);
            }
            else
            {
                control.Text = string.Empty;
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void typeComboBox_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = e.Value.ToString().Replace("_", " ");
        }

        private TransactionDTO CreateTransaction()
        {
            TransactionDTO transactionDTO = new TransactionDTO();
            //BasicTransaction transaction = null;
            TRANSACTION_TYPE income_expense_type;

            DateTime date = date_transaction.Value;
            Category category = (Category)cmb_category.SelectedItem;
            double amount = double.Parse(txt_amount.Text);
            string type = (string)cmb_type.SelectedItem;


            if (Enum.TryParse(type, out TransactionViewData.TYPE transactionType))
            {
                income_expense_type = transactionType.Equals(TransactionViewData.TYPE.INCOME) ||
                                      transactionType.Equals(TransactionViewData.TYPE.RECEIVABLE) ||
                                      transactionType.Equals(TransactionViewData.TYPE.RECURRING_INCOME)
                                      ? TRANSACTION_TYPE.INCOME : TRANSACTION_TYPE.EXPENSE;

                if (transactionType.Equals(TransactionViewData.TYPE.RECURRING_EXPENSE) ||
                    transactionType.Equals(TransactionViewData.TYPE.RECURRING_INCOME))
                {
                    Enum.TryParse((string)cmb_cycle.SelectedItem, out TransactionCycle cycle);
                    DateTime endDate = date_end.Value;

                    transactionDTO.Cycle = cycle;
                    transactionDTO.ExpectedEndDate = endDate;
                    transactionDTO.DTO_type = TransactionDTO.DTO_TYPE.Recurr;

                    //transaction = new RecurTransaction(0, amount, date, income_expense_type, category.Id, cycle, endDate);
                }
                else if (transactionType.Equals(TransactionViewData.TYPE.PAYABALE) ||
                    transactionType.Equals(TransactionViewData.TYPE.RECEIVABLE))
                {
                    Contact contact = (Contact)cmb_contact.SelectedItem;
                    DateTime expectedDate = date_expected.Value;

                    transactionDTO.ContactId = contact.Id;
                    transactionDTO.ExpectedDate = expectedDate;
                    transactionDTO.DTO_type = TransactionDTO.DTO_TYPE.Credit;

                    //transaction = new CreditTransaction(0, amount, date, income_expense_type, category.Id, contact.Id, expectedDate);

                }
                else
                {
                    
                    transactionDTO.DTO_type = TransactionDTO.DTO_TYPE.Basic;

                    //transaction = new BasicTransaction(0, amount, date, income_expense_type, category.Id);
                }

                transactionDTO.Type = income_expense_type;
            }

            transactionDTO.Id = _transaction.Id;
            transactionDTO.Amount = amount;
            transactionDTO.Date = date;
            transactionDTO.CategoryId = category.Id;
            return transactionDTO;
        }

        private bool ValidateTransaction()
        {
            if(string.IsNullOrWhiteSpace(txt_amount.Text))
            {
                return false;
            }

            if(cmb_cycle.Enabled && cmb_cycle.SelectedIndex < 0)
            {
                return false;
            }

            return true;
        }
        private void btn_update_Click(object sender, EventArgs e)
        {
            if (ValidateTransaction())
            {
                _transactionView.UpdateTransaction(CreateTransaction());
                //MessageBox.Show("Transaction Updated Successfully.");
                Close();
            }
            else
            {
                MessageBox.Show("One or more fields are empty");
            }
        }
    }
}
