﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyBudget.Core
{
    public class XMLHandler
    {
        public static T DeserializeToObjects<T>(string filepath)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            using (StreamReader sr = new StreamReader(filepath))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        public static void SerializeToXml<T>(T obj, string xmlFilePath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());

            using (StreamWriter writer = new StreamWriter(xmlFilePath))
            {
                xmlSerializer.Serialize(writer, obj);
            }
        }
    }
}
