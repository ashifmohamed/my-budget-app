﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Core
{
    public class JSONHandler
    {
        public static T DeserializeObject<T>(string json)
        {
            var deserializedObject = JsonConvert.DeserializeObject<T>(json);
            return deserializedObject;
        }
        public static List<T> DeserializeObjects<T>(string json)
        {
            var deserializedObjects = JsonConvert.DeserializeObject<List<T>>(json);
            return deserializedObjects;
        }
        public static string SerializeObject<T>(T obj)
        {
            var serializedObject = JsonConvert.SerializeObject(obj, Formatting.Indented);
            return serializedObject;
        }

        public static string SerializeObjects<T>(IList<T> obj)
        {
            var serializedObjects = JsonConvert.SerializeObject(obj, Formatting.Indented);
            return serializedObjects;
        }
    }
}
