﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MyBudget.Models.Transactions.RecurTransaction;

namespace MyBudget.Models.Transactions
{
    public class TransactionDTO
    {
        public enum DTO_TYPE
        {
            Basic,
            Credit,
            Recurr
        }
        public DTO_TYPE DTO_type { get; set; }
        public int Id { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public BasicTransaction.TRANSACTION_TYPE Type { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int ContactId { get; set; }
        public Contact Contact { get; set; }
        public DateTime ExpectedDate { get; set; }
        public RecurTransaction.TransactionCycle Cycle { get; set; }
        public DateTime ExpectedEndDate { get; set; }

        public static TransactionDTO Build(BasicTransaction basicTransaction)
        {
            TransactionDTO transactionDTO = new TransactionDTO
            {
                DTO_type = DTO_TYPE.Basic,
                Id = basicTransaction.Id,
                Amount = basicTransaction.Amount,
                Date = basicTransaction.Date,
                Category = basicTransaction.Category,
                CategoryId = basicTransaction.CategoryId,
                Type = basicTransaction.Type
            };

            return transactionDTO;
        }

        public TransactionDTO WithCreditTransaction(Contact contact, DateTime expectedDate)
        {
            this.DTO_type = DTO_TYPE.Credit;
            this.Contact = contact;
            this.ContactId = contact.Id;
            this.ExpectedDate = expectedDate;

            return this;
        }

        public TransactionDTO WithRecurTransaction(TransactionCycle cycle, DateTime expectedEndDate)
        {
            this.DTO_type = DTO_TYPE.Recurr;
            this.Cycle = cycle;
            this.ExpectedEndDate = expectedEndDate;

            return this;
        }
        public BasicTransaction MapToObject()
        {
            if(this.DTO_type == DTO_TYPE.Credit)
            {
                return new CreditTransaction(this.Id, this.Amount, this.Date,
                this.Type, this.Category, this.Contact, this.ExpectedDate);
            }
            else if (this.DTO_type == DTO_TYPE.Recurr)
            {
                return new RecurTransaction(this.Id, this.Amount, this.Date,
                this.Type, this.Category, this.Cycle, this.ExpectedEndDate);
            }
            else
            {
                return new BasicTransaction(this.Id, this.Amount, this.Date,
                 this.Type, this.Category);  
            }
        }
    }
}
