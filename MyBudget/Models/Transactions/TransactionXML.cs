﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyBudget.Models.Transactions
{
    [XmlRoot("Transactions")]
    public class TransactionXML
    {
        public TransactionXML()
        {
        }
        //public TransactionXML(List<BasicTransaction> list)
        //{
        //    transactionList = list;
        //}

        public TransactionXML(TransactionList list)
        {
            TransactionList = list;
        }


        //public List<BasicTransaction> transactionList { get; set; }
        //[XmlElement("BasicTransactions", typeof(List<BasicTransaction>))]
        //[XmlElement("CreditTransactions", typeof(List<CreditTransaction>))]
        //[XmlElement("RecurTransactions", typeof(List<RecurTransaction>))]
        public TransactionList TransactionList { get; set; }
    }
}
