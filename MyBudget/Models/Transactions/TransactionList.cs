﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyBudget.Models.Transactions
{
    public class TransactionList
    {
        public TransactionList()
        {
        }

        public TransactionList(List<BasicTransaction> basicTransactions, List<CreditTransaction> creditTransactions, 
            List<RecurTransaction> recurTransactions)
        {
            BasicTransactions = basicTransactions;
            CreditTransactions = creditTransactions;
            RecurTransactions = recurTransactions;
        }

        [XmlElement("BasicTransactions", typeof(List<BasicTransaction>))]
        public List<BasicTransaction> BasicTransactions { get; set; }

        [XmlElement("CreditTransactions", typeof(List<CreditTransaction>))]
        public List<CreditTransaction> CreditTransactions { get; set; }

        [XmlElement("RecurTransactions", typeof(List<RecurTransaction>))]
        public List<RecurTransaction> RecurTransactions { get; set; }

        public void Add(BasicTransaction transaction)
        {
            if(transaction is CreditTransaction creditTransaction)
            {
                CreditTransactions.Add(creditTransaction);
            }
            else if (transaction is RecurTransaction recurTransaction)
            {
                RecurTransactions.Add(recurTransaction);
            }
            else
            {
                BasicTransactions.Add(transaction);
            }
        }


    }
}
