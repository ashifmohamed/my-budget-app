﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Models
{
    public class TranscationChartData
    {
        public TranscationChartData()
        {
        }

        public string DateGroup { get; set; }
        public double TotalIncome { get; set; }
        public double TotalExpense { get; set; }
    }
}
