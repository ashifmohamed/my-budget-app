﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Models
{
    public class CategoryMonthTotalData
    {
        public CategoryMonthTotalData()
        {
        }
        public string Category { get; set; }
        public double Total { get; set; }
    }
}
