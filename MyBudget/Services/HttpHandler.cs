﻿using MyBudget.Core;
using MyBudget.Models.Transactions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyBudget.Services
{
    public sealed class HttpHandler
    {
        private static readonly Lazy<HttpHandler> lazy =
            new Lazy<HttpHandler>(() => new HttpHandler());

        private static readonly HttpClient HttpClient = new HttpClient();

        public static HttpHandler Instance { get { return lazy.Value; } }

        public async Task<List<T>> GetList<T>(string uri, Dictionary<string, string> queryParams)
        {
            try
            {
                var dictFormUrlEncoded = new FormUrlEncodedContent(queryParams);
                var queryString = await dictFormUrlEncoded.ReadAsStringAsync();
                HttpResponseMessage response = await HttpClient.GetAsync($"{uri}?{queryString}");
                response.EnsureSuccessStatusCode();

                var data = await response.Content.ReadAsAsync<List<T>>();
                return data;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.ToString());
                return default;
            }
        }

        public async Task<T> Get<T>(string uri, Dictionary<string, string> queryParams)
        {
            try
            {
                var dictFormUrlEncoded = new FormUrlEncodedContent(queryParams);
                var queryString = await dictFormUrlEncoded.ReadAsStringAsync();
                HttpResponseMessage response = await HttpClient.GetAsync($"{uri}?{queryString}");
                response.EnsureSuccessStatusCode();

                var data = await response.Content.ReadAsAsync<T>();
                return data;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.ToString());
                return default;
            }
        }

        public async Task<List<T>> GetList<T>(string uri)
        {
            try
            {
                HttpResponseMessage response = await HttpClient.GetAsync(uri);
                response.EnsureSuccessStatusCode();

                var data = await response.Content.ReadAsAsync<List<T>>();
                return data;
               // return await response.Content.ReadAsAsync<List<T>>();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.ToString());
                return default;
            }
        }

        public async Task<T> Get<T>(string uri)
        {
            try
            {
                HttpResponseMessage response = await HttpClient.GetAsync(uri);
                response.EnsureSuccessStatusCode();

                var data = await response.Content.ReadAsAsync<T>();
                return data;
                // return await response.Content.ReadAsAsync<List<T>>();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.ToString());
                return default;
            }
        }

        public async Task<bool> Post<T>(string uri, T content)
        {
            try
            {
                string contentString = JsonConvert.SerializeObject(content);
                var data = new StringContent(contentString, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await HttpClient.PostAsync(uri, data);

                if(response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var responseMsg = await response.Content.ReadAsStringAsync();
                    MessageBox.Show(responseMsg);
                    return false;
                }
                //response.EnsureSuccessStatusCode();
                //return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public async Task<bool> PostList<T>(string uri, IList<T> contents)
        {
            try
            {
                //JsonSerializerSettings settings = new JsonSerializerSettings
                //{
                //    TypeNameHandling = TypeNameHandling.All
                //};

                string contentString = JsonConvert.SerializeObject(contents);
                var data = new StringContent(contentString, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await HttpClient.PostAsync(uri, data);

                response.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        public async Task<bool> Put<T>(string uri, T content)
        {
            try
            {
                string contentString = JsonConvert.SerializeObject(content);
                var data = new StringContent(contentString, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await HttpClient.PutAsync(uri, data);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var responseMsg = await response.Content.ReadAsStringAsync();
                    MessageBox.Show(responseMsg);
                    return false;
                }
                //response.EnsureSuccessStatusCode();
                //return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        public async Task<T> Delete<T>(string uri)
        {
            try
            {
                //string contentString = JsonConvert.SerializeObject(content);
                //var data = new StringContent(contentString, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await HttpClient.DeleteAsync(uri);

                response.EnsureSuccessStatusCode();

                var data = await response.Content.ReadAsAsync<T>();
                return data;
                //return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.ToString());
                return default;
            }
        }

        private HttpHandler()
        {
            string url = ConfigurationManager.AppSettings.Get("MyBudget.API.URI");

            //HttpClient = new HttpClient
            //{
            //    BaseAddress = new Uri(url)
            //};
            HttpClient.BaseAddress = new Uri(url);
            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

    }
}
