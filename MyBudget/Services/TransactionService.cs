﻿using MyBudget.Data;
using MyBudget.Models.Transactions;
using MyBudget.Views.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MyBudget.Models.Transactions.BasicTransaction;

namespace MyBudget.Services
{
    public class TransactionService
    {
        private readonly string PATH = "TransactionService";
        public void InitializeData()
        {
           // TransactionData.InitializeData();
        }
        public async Task<TransactionDTO> GetTransactionAsync(int id)
        {
            string url = string.Format("{0}/{1}", PATH, id.ToString());
            return await HttpHandler.Instance.Get<TransactionDTO>(url);
        }
        public async Task<IList<BasicTransaction>> GetAllTransactionsAsync()
        {
            return await HttpHandler.Instance.GetList<BasicTransaction>(PATH);
        }
        public async Task<List<BasicTransaction>> GetAllTransactionsAsync(DateTime start, DateTime end)
        {
            var queryParams = new Dictionary<string, string> 
            { 
                { "start", DateToString(start) },
                { "end", DateToString(end) }
            };

            return await HttpHandler.Instance.GetList<BasicTransaction>(PATH, queryParams);
        }

        public async Task<bool> AddTransactions(IList<BasicTransaction> transactions)
        {
            return await HttpHandler.Instance.PostList<BasicTransaction>(PATH, transactions);
            //TransactionData.Insert(transactions);
        }
        public async Task<bool> AddTransactions(TransactionList transactions)
        {
            return await HttpHandler.Instance.Post<TransactionList>(PATH, transactions);
            //TransactionData.Insert(transactions);
        }
        public async Task<bool> UpdateTransactionAsync(TransactionDTO transaction)
        {
            string url = string.Format("{0}/{1}", PATH, transaction.Id.ToString());
            return await HttpHandler.Instance.Put<TransactionDTO>(url, transaction);
            //return TransactionData.Update(transaction);
        }

        public async Task<BasicTransaction> RemoveTransactionAsync(int id)
        {
            string url = string.Format("{0}/{1}", PATH, id.ToString());
            return await HttpHandler.Instance.Delete<BasicTransaction>(url);
            //return TransactionData.Delete(id);
        }

        //public IList<BasicTransaction> GetFilteredTransactions(DateTime start, DateTime end,
        //    TRANSACTION_TYPE type, bool isRecurring, bool isCredit)
        //{
        //    return TransactionData.GetFilteredData(start, end, type, isRecurring, isCredit);
        //}

        public async Task<List<TransactionDTO>> GetFilteredTransactionsAsync(DateTime start, DateTime end,
            TransactionViewData.TYPE type)
        {
            string typeString = Enum.GetName(typeof(TransactionViewData.TYPE), type);
            string url = string.Format("{0}/{1}", PATH, "filter");

            var queryParams = new Dictionary<string, string>
            {
                { "start", DateToString(start) },
                { "end", DateToString(end) },
                { "type", typeString },
            };

            return await HttpHandler.Instance.GetList<TransactionDTO>(url, queryParams);
            //return TransactionData.GetFilteredData(start, end, TRANSACTION_TYPE.EXPENSE, true, false);
        }

        private string DateToString(DateTime date)
        {
            return date.ToString("MM/dd/yyyy");
        }
    }
}
