﻿using MyBudget.Data;
using MyBudget.Models;
using MyBudget.Models.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MyBudget.Models.Dashboard;
using static MyBudget.Models.Transactions.BasicTransaction;

namespace MyBudget.Services
{
    public class DashboardService
    {
        private readonly string PATH = "TransactionService";

        //public IList<BasicTransaction> GetFilteredTransactions(DateTime start, DateTime end,
        //    TRANSACTION_TYPE type, bool isRecurring, bool isCredit)
        //{
        //    return TransactionData.GetFilteredData(start, end, type, isRecurring, isCredit);
        //}

        public async Task<IList<TranscationChartData>> GetTransactionChartData(DateTime start, 
            DateTime end, CHART_GROUP group)
        {
            string groupString = Enum.GetName(typeof(CHART_GROUP), group);
            string url = string.Format("{0}/{1}", PATH, "chart");

            var queryParams = new Dictionary<string, string>
            {
                { "start", DateToString(start) },
                { "end", DateToString(end) },
                { "group", groupString },
            };

            return await HttpHandler.Instance.GetList<TranscationChartData>(url, queryParams);
            //return TransactionData.GetTransactionChartData(start, end, group);
        }
        public async Task<IList<CategoryMonthTotalData>> GetCategoryChartData(DateTime date)
        {
            string url = string.Format("{0}/{1}", "CategoryService", "chart");

            var queryParams = new Dictionary<string, string>
            {
                { "date", DateToString(date) },
            };

            return await HttpHandler.Instance.GetList<CategoryMonthTotalData>(url, queryParams);
            //return TransactionData.GetCategoryChartData(start, end, group);
        }

        public async Task<Double> GetForecastBalance(DateTime currentDate, TRANSACTION_TYPE type)
        {
            string typeString = Enum.GetName(typeof(TRANSACTION_TYPE), type);
            string url = string.Format("{0}", "ForecastService");
            //string url = string.Format("{0}/{1}", "ForecastService", "forecast");

            var queryParams = new Dictionary<string, string>
            {
                { "date", DateToString(currentDate) },
                { "type", typeString },
            };

            string result = await HttpHandler.Instance.Get<string>(url, queryParams);
            bool parsed = Double.TryParse(result, out double balance);
            return parsed ? balance : 0;
            //return TransactionData.GetForecastBalance(currentDate, type);
        }
        public async Task<Double> GetActualBalance(DateTime currentDate, TRANSACTION_TYPE type)
        {
            string typeString = Enum.GetName(typeof(TRANSACTION_TYPE), type);
            string url = string.Format("{0}/{1}", PATH, "balance");

            var queryParams = new Dictionary<string, string>
            {
                { "date", DateToString(currentDate) },
                { "type", typeString },
            };

            string result = await HttpHandler.Instance.Get<string>(url, queryParams);
            bool parsed = Double.TryParse(result, out double balance);
            return parsed ? balance : 0;
            //return TransactionData.GetActualBalance(currentDate, type);
        }

        private string DateToString(DateTime date)
        {
            return date.ToString("MM/dd/yyyy");
        }

    }
}
