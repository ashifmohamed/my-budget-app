﻿using MyBudget.Data;
using MyBudget.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Services
{
    public class CategoryService
    {
        private readonly string PATH = "CategoryService";
        public void InitializeData()
        {
            //CategoryData.InitializeData();
        }
        public async Task<IList<Category>> GetAllCategoriesAsync()
        {

            //List<Category> categories = null;
            //try
            //{
            //    HttpResponseMessage response = await HttpHandler.HttpClient.GetAsync(PATH).ConfigureAwait(false);
            //    if (response.IsSuccessStatusCode)
            //    {
            //        categories = await response.Content.ReadAsAsync<List<Category>>();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //return categories;
            return await HttpHandler.Instance.GetList<Category>(PATH);
        }

        public async Task<bool> AddCategoryAsync(string name)
        {
            Category category = new Category(name);
            return await HttpHandler.Instance.Post<Category>(PATH, category);

            //return CategoryData.Insert(new Category(name));
        }

        public async Task<bool> UpdateCategoryAsync(Category category)
        {
            string url = string.Format("{0}/{1}",PATH, category.Id.ToString());
            return await HttpHandler.Instance.Put<Category>(url, category);
        }

    }
}
