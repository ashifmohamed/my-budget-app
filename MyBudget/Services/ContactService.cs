﻿using MyBudget.Data;
using MyBudget.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBudget.Services
{
    public class ContactService
    {
        private readonly string PATH = "ContactService";
        public void InitializeData()
        {
            //ContactData.InitializeData();
        }
        public async Task<IList<Contact>> GetAllContactsAsync()
        {
            return await HttpHandler.Instance.GetList<Contact>(PATH);
        }

        public async Task<bool> AddContactAsync(string name)
        {
            Contact contact = new Contact(name);
            return await HttpHandler.Instance.Post<Contact>(PATH, contact);
            //return ContactData.Insert(new Contact(name));
        }
        public async Task<bool> UpdateContactAsync(Contact contact)
        {
            string url = string.Format("{0}/{1}", PATH, contact.Id.ToString());
            return await HttpHandler.Instance.Put<Contact>(url, contact);
           // return ContactData.Update(contact);
        }
    }
}
